const assert = require('assert');
const http = require('http');
const app = require('../routes/todos.js');

describe('Todos API', () => {
  it('should return status code 200 for GET /todos', (done) => {
    const server = http.createServer(app);
    
    server.listen(0, () => {
      const port = server.address().port;

      http.get(`http://localhost:3000/todos`, (res) => {
        assert.strictEqual(res.statusCode, 200);
        server.close();
        done();
      });
    });
  });
});
